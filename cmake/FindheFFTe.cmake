# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
FindheFFTe
----------

Find the heFFTe library

Imported Targets
^^^^^^^^^^^^^^^^

This module provides the following imported targets, if found:

``heFFTe::heFFTe``
  The heFFTe library

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

``heFFTe_FOUND``
  True if the system has found the heFFTe library.
``heFFTe_VERSION``
  The version of the heFFTe library which was found.
``heFFTe_INCLUDE_DIRS``
  Include directories needed to use heFFTe.
``heFFTe_LIBRARIES``
  Libraries needed to link to heFFTe.

Cache Variables
^^^^^^^^^^^^^^^

None yet.

#]=======================================================================]

find_path(heFFTe_INCLUDE_DIRS
  NAMES heffte.h
  PATHS $ENV{HEFFTE_INC}
)
find_library(heFFTe_LIBRARIES
  NAMES heffte
  PATHS $ENV{HEFFTE_LIB}
)

set(heFFTe_VERSION "2.0.0")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(heFFTe
  FOUND_VAR heFFTe_FOUND
  REQUIRED_VARS
    heFFTe_LIBRARIES
    heFFTe_INCLUDE_DIRS
  VERSION_VAR heFFTe_VERSION
)

if(heFFTe_FOUND AND NOT TARGET heFFTe:heFFTe)
  add_library(heFFTe::heFFTe UNKNOWN IMPORTED)
  set_target_properties(heFFTe::heFFTe PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${heFFTe_INCLUDE_DIRS}"
    IMPORTED_LOCATION "${heFFTe_LIBRARIES}"
  )
endif()

mark_as_advanced(
  heFFTe_INCLUDE_DIR
  heFFTe_LIBRARY
)

# compatibility variables
set(heFFTe_VERSION_STRING ${heFFTe_VERSION})
