/*
    -- FBI - ICL --
       Univ. of Tennessee, Knoxville
       @date
       Benchmark of a 3-D complex-to-complex FFT
*/

#define BENCH_INPUT std::complex<precision_type>
#define BENCH_C2C

#include "fbi_test3d.hpp"
