/*
    -- FBI - ICL --
       University of Tennessee, Knoxville, USA
       @date
       2-D and 3-D FFT benchmark
*/

#include <iostream>
#include <iomanip>

#include <mpi.h>

#include "fbi_utils.hpp"
#include "fbi_common.hpp"
#include "fbi_fft3d.hpp"

#include "fbi_backend_vector.hpp"
#include "fbi_data_gen.hpp"


/*!
 * \ingroup BenchmarkC2C
 * \brief Benchmark for FFT computation
 *
 * \param size_fft given as a 3-coordinate vector (nx, ny, nz). For 2-D transform, set nz = 1
 *
 */
template<typename backend_tag, typename precision_type, typename index>
void benchmark_fft(std::array<int,3> size_fft, std::deque<std::string> const &args){

    int me, nprocs;
    MPI_Comm fft_comm = MPI_COMM_WORLD;  // Change if need to compute FFT within a subcommunicator
    MPI_Comm_rank(fft_comm, &me);
    MPI_Comm_size(fft_comm, &nprocs);

    // Create input and output boxes on local processor
    fbi::box3d<index> const world = {{0, 0, 0}, {size_fft[0]-1, size_fft[1]-1, size_fft[2]-1}};

    // Get grid of processors at input and output
    std::array<int,3> proc_i = fbi::proc_setup_min_surface(world, nprocs);
    std::array<int,3> proc_o = fbi::proc_setup_min_surface(world, nprocs);

    // Check if user in/out processor grids are pencil-shaped, useful for performance comparison with other libraries
    if (io_pencils(args)){
        std::array<int, 2> proc_grid = fbi::make_procgrid(nprocs);
        proc_i = {1, proc_grid[0], proc_grid[1]};
        proc_o = {1, proc_grid[0], proc_grid[1]};
    }

    std::vector<fbi::box3d<index>> inboxes  = fbi::split_world(world, proc_i);
    std::vector<fbi::box3d<index>> outboxes = fbi::split_world(world, proc_o);

    // Define FFT plan and workspace according to each library
    // 1. FFT definition - heFFTe library:
    auto fft = fbi::make_fft3d<backend_tag>(inboxes[me], outboxes[me], fft_comm);
    typename fbi::fft3d<backend_tag>::template buffer_container<std::complex<precision_type>> workspace(fft.size_workspace());

    /**
    2. In general, we will have data-structure of library-options that can be read from command line; for example:
    * * auto options = args_to_options<backend_tag>(args);
    * * auto fft = make_fft3d<backend_tag>(inboxes[me], outboxes[me], fft_comm, options); 
    *
    */

    // Create grid of processors
    std::array<int, 2> proc_grid = fbi::make_procgrid(nprocs);
    // writes out the proc_grid in the given dimension
    auto print_proc_grid = [&](int i){
        switch(i){
            case -1: std::cout << "(" << proc_i[0] << ", " << proc_i[1] << ", " << proc_i[2] << ")  "; break;
            case  0: std::cout << "(" << 1 << ", " << proc_grid[0] << ", " << proc_grid[1] << ")  "; break;
            case  1: std::cout << "(" << proc_grid[0] << ", " << 1 << ", " << proc_grid[1] << ")  "; break;
            case  2: std::cout << "(" << proc_grid[0] << ", " << proc_grid[1] << ", " << 1 << ")  "; break;
            case  3: std::cout << "(" << proc_o[0] << ", " << proc_o[1] << ", " << proc_o[2] << ")  "; break;
            default:
                throw std::runtime_error("printing incorrect direction");
        }
    };


    // Locally initialize input
    auto input = make_data<BENCH_INPUT>(inboxes[me]);
    auto reference_input = input; // safe a copy for error checking

    // define allocation for in-place transform
    std::vector<std::complex<precision_type>> output(std::max(fft.size_outbox(), fft.size_inbox()));
    std::copy(input.begin(), input.end(), output.begin());

    std::complex<precision_type> *output_array = output.data();
    #ifdef FBI_ENABLE_GPU
    gpu::vector<std::complex<precision_type>> gpu_output;
    if (std::is_same<backend_tag, gpu_backend>::value){
        gpu_output = gpu::transfer::load(output);
        output_array = gpu_output.data();
    }
    #endif

    // Warmup
    fft.forward(output_array, output_array,  fbi::scale::full);
    fft.backward(output_array, output_array);

    // Execution
    int const ntest = 5;
    MPI_Barrier(fft_comm);
    double t = -MPI_Wtime();
    for(int i = 0; i < ntest; ++i) {
        fft.forward(output_array, output_array, workspace.data(), fbi::scale::full);
        fft.backward(output_array, output_array, workspace.data());
    }
    // TODO commented if for now but needs to be fixed
    #ifdef FBI_ENABLE_GPU
    if (backend::uses_gpu<backend_tag>::value)
        gpu::synchronize_default_stream();
    #endif
    t += MPI_Wtime();
    MPI_Barrier(fft_comm);

    // Get execution time
    double t_max = 0.0;
    MPI_Reduce(&t, &t_max, 1, MPI_DOUBLE, MPI_MAX, 0, fft_comm);

    // Validate result
    #ifdef FBI_ENABLE_GPU
    if (std::is_same<backend_tag, gpu_backend>::value){
        // unload from the GPU, if it was stored there
        output = gpu::transfer::unload(gpu_output);
    }
    #endif
    output.resize(input.size()); // match the size of the original input

//  precision_type err = 0.0;
//  for(size_t i=0; i<input.size(); i++)
//      err = std::max(err, std::abs(input[i] - output[i]));
//  precision_type mpi_max_err = 0.0;
//  MPI_Allreduce(&err, &mpi_max_err, 1, fbi::mpi::type_from<precision_type>(), MPI_MAX, fft_comm);

//  if (mpi_max_err > precision<std::complex<precision_type>>::tolerance){
//      // benchmark failed, the error is too much
//      if (me == 0){
//          std::cout << "------------------------------- \n"
//               << "ERROR: observed error after FBI benchmark exceeds the tolerance\n"
//               << "       tolerance: " << precision<std::complex<precision_type>>::tolerance
//               << "  error: " << mpi_max_err << std::endl;
//      }
//      return;
//  }

//  // Print results
//  if(me==0){
//      t_max = t_max / (2.0 * ntest);
//      double const fftsize  = static_cast<double>(world.count());
//      double const floprate = 5.0 * fftsize * std::log(fftsize) * 1e-9 / std::log(2.0) / t_max;
//      long long mem_usage = static_cast<long long>(fft.size_inbox()) + static_cast<long long>(fft.size_outbox())
//                          + static_cast<long long>(fft.size_workspace());
//      mem_usage *= sizeof(std::complex<precision_type>);
//      mem_usage /= 1024ll * 1024ll; // convert to MB
//      std::cout << "\n----------------------------------------------------------------------------- \n";
//      std::cout << "3-D FFT performance test - FBI \n";
//      std::cout << "----------------------------------------------------------------------------- \n";
//      std::cout << "Backend:   " << backend::name<backend_tag>() << "\n";
//      std::cout << "Size:      " << world.size[0] << "x" << world.size[1] << "x" << world.size[2] << "\n";
//      std::cout << "MPI ranks: " << std::setw(4) << nprocs << "\n";
//      std::cout << "Grids: ";
//      std::cout << "Time per run: " << t_max << " (s)\n";
//      std::cout << "Performance:  " << floprate << " GFlops/s\n";
//      std::cout << "Memory usage: " << mem_usage << "MB/rank\n";
//      std::cout << "Tolerance:    " << precision<std::complex<precision_type>>::tolerance << "\n";
//      std::cout << "Max error:    " << mpi_max_err << "\n";
//      std::cout << std::endl;
//  }
}

template<typename backend_tag>
bool perform_benchmark(std::string const &precision_string, std::string const &backend_string, std::string const &backend_name,
                       std::array<int,3> size_fft, std::deque<std::string> const &args){
    if (backend_string == backend_name){
        if (precision_string == "float"){
            benchmark_fft<backend_tag, float, int>(size_fft, args);
        }else if (precision_string == "double"){
            benchmark_fft<backend_tag, double, int>(size_fft, args);
        }else if (precision_string == "float-long"){
            benchmark_fft<backend_tag, float, long long>(size_fft, args);
        }else{ // double-long
            benchmark_fft<backend_tag, double, long long>(size_fft, args);
        }
        return true;
    }
    return false;
}

int main(int argc, char *argv[]){

    MPI_Init(&argc, &argv);

    // 1. Define available 1-D FFT backends for benchmarking
    std::string backends = "";
    #ifdef FBI_ENABLE_FFTW
    backends += "fftw ";
    #endif
    #ifdef FBI_ENABLE_CUDA
    backends += "cufft ";
    #endif
    #ifdef FBI_ENABLE_ROCM
    backends += "rocfft ";
    #endif
    #ifdef FBI_ENABLE_MKL
    backends += "mkl ";
    #endif

    // 2. Read and validate the following parameters: 1-D backend, inputs data-type, and 3-D FFT size 
    std::array<int,3> size_fft = { 0, 0, 0 };

    std::string backend_string = argv[1];
    std::string precision_string = argv[2];

    if (precision_string != "float"      and precision_string != "double" and
        precision_string != "float-long" and precision_string != "double-long"){
        if (fbi::mpi::world_rank(0)){
            std::cout << "Invalid precision!\n";
            std::cout << "Must use float or double" << std::endl;
        }
        MPI_Finalize();
        return 0;
    }

    try{
        size_fft = { std::stoi(argv[3]), std::stoi(argv[4]), std::stoi(argv[5])};
        for(auto s : size_fft) if (s < 1) throw std::invalid_argument("negative input");
    }catch(std::invalid_argument &e){
        if (fbi::mpi::world_rank(0)){
            std::cout << "Cannot convert the sizes into positive integers!\n";
            std::cout << "Encountered error: " << e.what() << std::endl;
        }
        MPI_Finalize();
        return 0;
    }


    // 3. Run and validate benchmark
    bool valid_backend = false;
    #ifdef FBI_ENABLE_FFTW
    valid_backend = valid_backend or perform_benchmark<backend::fftw>(precision_string, backend_string, "fftw", size_fft);
    #endif
    #ifdef FBI_ENABLE_MKL
    valid_backend = valid_backend or perform_benchmark<backend::mkl>(precision_string, backend_string, "mkl", size_fft);
    #endif
    #ifdef FBI_ENABLE_CUDA
    valid_backend = valid_backend or perform_benchmark<backend::cufft>(precision_string, backend_string, "cufft", size_fft);
    #endif
    #ifdef FBI_ENABLE_ROCM
    valid_backend = valid_backend or perform_benchmark<backend::rocfft>(precision_string, backend_string, "rocfft", size_fft);
    #endif

    if (not valid_backend){
        if (fbi::mpi::world_rank(0)){
            std::cout << "Invalid backend " << backend_string << "\n";
            std::cout << "The available backends are: " << backends << std::endl;
        }
        MPI_Finalize();
        return 0;
    }

    MPI_Finalize();
    return 0;
}

