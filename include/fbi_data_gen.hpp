/*
    -- FBI - ICL --
       University of Tennessee, Knoxville, USA
       @date
*/

#ifndef FBI_DATA_GEN_HPP
#define FBI_DATA_GEN_HPP

#include <random>

#include "fbi_geometry.hpp"

/*!
 * \ingroup fft3d
 * \addtogroup Random data generator 
 */
template<typename scalar_type, typename index>
std::vector<scalar_type> make_data(fbi::box3d<index> const world){
    std::minstd_rand park_miller(4242);
    std::uniform_real_distribution<double> unif(0.0, 1.0);

    std::vector<scalar_type> result(world.count());
    for(auto &r : result)
        r = static_cast<scalar_type>(unif(park_miller));
    return result;
}

#endif
