#! /usr/bin/bash

p3dver=2.7.9
p3dfnm=${p3dver}.tar.gz
p3ddir=p3dfft-${p3dver}
urlbalse=https://github.com/sdsc/p3dfft/archive/refs/tags

if test -f $p3dfnm ; then
  echo Already donwloaded
else
  curl -OL ${urlbalse}/${p3dfnm}
fi

if test -d $p3ddir ; then
  echo Already unpacked
else
  tar -xzf ./$p3dfnm
fi

module purge

for mod in gcc/7.4.0 spectrum-mpi/10.3.1.2-20200121 lsf-tools/2.0 fftw/3.3.8
do
  module load $mod
done

module list

# remove Fortran libraries from C link line
sed -i.orig 's/..FORTRAN_LIB.//' $p3ddir/sample/C/Makefile.am

# configure.ac has host-specific logic for setting CCLD to either CC or FC depending on IBM/Intel/GNU/PGI compilers
# MPI's Fortran bindings library is wired to specific names for OpenMPI and MPICH
( cd ./$p3ddir ; autoreconf -ivf ;  env FC=mpif90 FCFLAGS="-O2 -I../build -I../include" CC=mpicc CFLAGS="-O2 -lgfortran -lm -lmpi_ibm_mpifh" ./configure --enable-gnu --enable-fftw --with-fftw=$OLCF_FFTW_ROOT --enable-measure --enable-openmpi; make -j )

# second attempt is required because Fortran modules are now build after the first make(1) invocation
( cd ./$p3ddir ; make -j )
