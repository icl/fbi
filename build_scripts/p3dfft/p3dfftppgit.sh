#! /usr/bin/bash

p3ddir=p3dfft.3
git clone https://github.com/sdsc/$p3ddir
git -C $p3ddir checkout 2f2d70d #  36b2ed0 is for CUDA

#mv $p3ddir/sample/C/test2D+empty.c $p3ddir/sample/C/test2D+empty_c.c

git -C $p3ddir checkout build/Makefile.am
patch --verbose -p1 -i p3dfftppgit.patch

for mod in gcc/7.4.0 spectrum-mpi/10.3.1.2-20200121 lsf-tools/2.0 fftw/3.3.8
do
  module load $mod
done

module list

# (cd $p3ddir ; autoreconf -ivf)

(cd ./$p3ddir ; env FFLAGS=-O2 CFLAGS=-O2 ./configure --enable-gnu --enable-fftw --with-fftw=$OLCF_FFTW_ROOT --enable-timers)

make -C ./$p3ddir -j
