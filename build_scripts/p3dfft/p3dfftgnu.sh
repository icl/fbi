#! /usr/bin/bash

p3dver=3.0.1
p3dfnm=v${p3dver}.tar.gz
p3ddir=p3dfft.3-${p3dver}

if test -f $p3dfnm ; then
  echo Already donwloaded
else
  curl -OL https://github.com/sdsc/p3dfft.3/archive/refs/tags/${p3dfnm}
fi

if test -d $p3ddir ; then
  echo Already unpacked
else
  tar -xzf ./$p3dfnm
fi

module purge

for mod in gcc/7.4.0 spectrum-mpi/10.3.1.2-20200121 lsf-tools/2.0 fftw/3.3.8
do
  module load $mod
done

module list

(cd ./$p3ddir ; env FFLAGS=-O2 CFLAGS=-O2 ./configure --enable-gnu --enable-fftw --with-fftw=$OLCF_FFTW_ROOT ; make -j)
