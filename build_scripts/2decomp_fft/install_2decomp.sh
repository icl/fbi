#!/bin/bash
# Script to install 2decomp&FFT
# Written by sebastien.cayrols@icl.utk.edu
# Issue: sebastien.cayrols@icl.utk.edu
#
#
# This script installs and fixed the compilation issue of the
# library in the $source_root folder.
#
# In details, the script 
# 1_ downloads the source,
# 2_ extracts the library,
# 3_ loads some modules related to the content on the src/Makefile.inc created in 4_,
# 4_ creates and patches the src/Mkefile.inc,
# 5_ fixes the compilation issues
# 6_ installs the library.
#
# A few parameters may need to be updated.
# For example, updating default_prefix will change the location where the library is extracted.


#====================
# Parameters
default_prefix=$PWD
source_root=${PREFIX:-$default_prefix}
#List of modules to load
modules=( "xl/16.1.1-10" "spectrum-mpi/10.3.1.2-20200121" "fftw/3.3.8" )

url=http://www.2decomp.org/download/2decomp_fft-1.5.847.tar.gz

# Patch applied to the Makefile.inc to comply with Summit
makeInc_patchname=2decomp_fft_makefile_inc.patch

#=================
#=================
# **** Nothing should not be updated below this line ****
#=================
#=================

#Extract info from the url
tar_filename=$(basename $url)
lib_basename=${tar_filename%.tar.gz}
lib_name=$(echo $lib_basename | cut -d '-' -f 1)
lib_root=$source_root/$lib_basename

#List of folder to update
folders=( "src" "examples" )
#Enforce the use of a single job in make
make_nrunner=1

# Function that displays an error message and stop the execution
function failed {
  echo "$1"
  exit 1
}


#===================================
# Check whether the sources already exist
if [ ! -e $lib_root ]; then
  # Download and extract the source
  [[ -e $tar_filename ]] || wget $url \
    || failed "Failed to download the archive at $url"

  [[ -e $lib_basename ]] || tar -xzvf $tar_filename \
    || failed "Failed to extract the source from $tar_filename"

  # Move into the source folder
  mv $lib_name $lib_root \
    || failed "Failed to rename the folder $lib_name into $lib_root"

  cp $makeInc_patchname $lib_root \
    || failed "Failed to copy $makeInc_patchname in "
fi

#===================================
# Load the 
module purge
for mod in ${modules[@]}; do
  echo "Load module $mod"
  module load $mod \
    || failed "Failed to load module $mod"
done

cd $lib_root

#=============================
# Fix compilation issue
# 1: for each file we change the extension from f90 to F90 that
#     allows the application of the preprocessor
# 2: Update the list of source files in the Makefiles
for dir in ${folders[@]}; do
  for filename in $(find ./$dir -name "*.f90"); do
    # First change the extension of the included files
    sed -i 's/\.f90/\.F90/g' $filename

    # Change the extension of the file itself
    filename_base=${filename%.f90}
    mv $filename ${filename_base}.F90
  done
done

for dir in ${folders[@]}; do
  for filename in $(find ./$dir -name "Makefile"); do
    sed -i 's/\.f90/\.F90/g' $filename
  done
done


# Create the Makefile.inc from one of the examples provided
cp src/Makefile.inc.x86 src/Makefile.inc

# Patch it to comply with Summit needs
patch src/Makefile.inc $makeInc_patchname \
  || failed "Failed to apply the patch $makeInc_patchname"

#set +x; failed 'Stop execution'
make -j $make_nrunner
