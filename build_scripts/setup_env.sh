#!/bin/sh

# This script is to be sourced into your environment before running 
# the library build scripts. It should include mostly loading
# of required modules.

module purge

# Example "module load", please change to be more specific
module load cuda

