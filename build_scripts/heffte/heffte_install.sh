#!/bin/bash
# Intall heFFTe
# Alan Ayala

# CMake compilation (requires at least version 3.10)
module purge
module load cuda
module load cmake
module load mpi
module load fftw
git clone https://bitbucket.org/icl/heffte.git
cd heffte
mkdir build; $_ 
cmake -DHeffte_ENABLE_MKL=ON -DHeffte_ENABLE_FFTW=ON -DHeffte_ENABLE_CUDA=ON -DCMAKE_BUILD_TYPE="-O3" ..

# Once library is compiled, you can find performance executables at build/benchmarks/
# Two executables get created: speed3d_c2c and speed3d_r2c, respectively, for complex-to-complex and real-to-complex transforms.

# See ../run_scripts/heffte/ for examples in how to use these testers.