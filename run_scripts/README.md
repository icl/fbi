# Running each library on Summit

This folder contains instructions for benchmarking state-of-the-art parallel FFTT libraries. We compute strong scalability tests for a 3-D comples-to-complex FFT of size 1024^3.

In the corresponding folder, please create a README which includes the following (see *heffte/* for an example):

- Make sure the executable performs 4 reshapes, and that the tester reports the time for each iteration (at least use 10 iterations).

- Provide the name and path to the executable for computing a 3-D complex-to-complex FFT.

- Document the flags and running options.

# Network setup

We will use the following network configuration for our benchmarks:

~~~
export PAMI_IBV_DEVICE_NAME="mlx5_0:1,mlx5_3:1"
export PAMI_IBV_DEVICE_NAME_1="mlx5_3:1,mlx5_0:1"
export PAMI_ENABLE_STRIPING=1
~~~

# Scalability test

We will perform the following scalability test for *CPU-based* libraries:

~~~
p=10
for i in $(seq 7 $p); do
    nodes=$((40*2**$i))
    jsrun -n$nodes -r40 -a1 -c1 <CPU_executable> <options>
done
~~~

Similarly, for the *GPU* case:

~~~
p=10
for i in $(seq 7 $p); do
    nodes=$((6*2**$i))
    jsrun  -n$nodes -r6 -g1 -a1 -c1 <GPU_executable> <options>
done
~~~

