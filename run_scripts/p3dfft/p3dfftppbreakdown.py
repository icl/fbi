#! /usr/bin/env python3

# from __future__ import annotations
from __future__ import absolute_import, division, generator_stop, \
    generators, nested_scopes, print_function, unicode_literals, with_statement

import sys
if sys.version_info < (3,):
    raise RuntimeError("Only Python 3 supported")


def safemap(f, l):
    newl = list()
    for i in l:
        try:
            newl.append(f(i))
        except:
            continue
    return newl


def cleanup(line):
    for replacement in "()":
        line = line.replace(replacement, "")
    return line


def process(fn):
    for line in open(fn):
        if line.lower().find("processor grid") >= 0:
            l = safemap(int, line.split())
            if 0:print(l[0]*l[1], l[0], l[1], l[0]*1./l[1], end=" ")

        elif line.lower().find("c2c_inplace.c") >= 0:
            data_key = "c2ci"
        elif line.lower().find("c2c.c") >= 0:
            data_key = "c2co"

        elif line.lower().find("transform time") >= 0:
            l = safemap(float, line.split())
            if 0:print(l[0])

        for unqstr, new, fcn in (
            ("c2c_inplace.c", True, lambda orig, lwr, ff: [0]),
            ("c2c.c",         True, lambda orig, lwr, ff: [1]),
            ("processor grid",False,lambda orig, lwr, ff: [ff[0]*ff[1], ff[0], ff[1], ff[0]/ff[1]]),
            ("transform time",False,lambda orig, lwr, ff: [ff[0]]),
            ("reorder_trans", False,lambda orig, lwr, ff: [ff[0]]),
            ("reorder_out",   False,lambda orig, lwr, ff: [ff[0]]),
            ("reorder_in",    False,lambda orig, lwr, ff: [ff[0]]),
            ("trans_exec",    False,lambda orig, lwr, ff: [ff[0]]),
            ("packsend ",     False,lambda orig, lwr, ff: [ff[0]]),
            ("packsend_trans",False,lambda orig, lwr, ff: [ff[0]]),
            ("unpackrecv",    False,lambda orig, lwr, ff: [ff[0]]),
            ("alltoall",      False,lambda orig, lwr, ff: [ff[0]]),
            ):
            if unqstr in line.lower():
                if new:
                    try:
                        print(" ".join(map(str, fields)))
                    except UnboundLocalError:
                        pass
                    fields = list()

                fields.extend(fcn(line, line.lower(), safemap(float, cleanup(line).split())))

    print(" ".join(map(str, fields)))


def main(argv):
    for fn in argv[1:]:
        process(fn)
    if len(argv) <= 1:
        process(__file__.replace(".py", ".txt"))
    return 0


if "__main__" == __name__:
    sys.exit(main(sys.argv))
