#! /usr/bin/env python3

# from __future__ import annotations
from __future__ import absolute_import, division, generator_stop, \
  generators, nested_scopes, print_function, unicode_literals, with_statement

import sys

def safemap(f, l):
  newl = list()
  for i in l:
    try:
      newl.append(f(i))
    except:
      continue
  return newl

def process(fn):
  count = 0
  for line in open(fn):
    count += 1
    if count % 1000000 == 0:
      if 0: print("Line", count)
    if line.lower().find("processor grid") >= 0:
      l = safemap(int, line.split())
      print(l[0]*l[1], l[0], l[1], l[0]*1./l[1], end=" ")
    elif line.lower().find("transform time") >= 0:
      l = safemap(float, line.split())
      print(l[0])

def main(argv):
  for fn in argv[1:]:
    process(fn)
  return 0

if "__main__" == __name__:
  sys.exit(main(sys.argv))
