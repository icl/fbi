# Testing P3DFFT++

After successful compilation with either `p3dfftgnu.sh` or `p3dfftxlf.sh`, run
`p3dfft.3-3.0.1/samples/C++/test3D_c2c_cpp` for complex-to-complex transform. 
`p3dgnu1024pp.lsf` is the submission script of Summit with all options set to
test 1024^3 grid.

Configuration of execution of sample files is done with two files: `stdin`
and `dims`. Both files have exactly one line with the relevant parameters.

The `stdin` file has the following parameters in first line:

- Nx is input dimension along X axis (positive integer)
- Ny is input dimension along Y axis (positive integer)
- Nz is input dimension along Z axis (positive integer)
- Ndim is the number of dimensions; 1 represents 1D process grid and
  2 represents 2D process grid that will be read from `dims` file
  (either 1 or 2)
- Nrep is the number of repetitions of transforms in a single
  run to report an average value. (positive integer)
  
The `dims` file has the following two parameters in first line:

1. Px is the number of row processes in the 2D global process grid
2. Py is the number of column processes in the 2D global process grid
