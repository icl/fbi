# Compilation
  - put this file in the folder examples/timing,
  - update the Makefile by adding the file,
  - compile using make.

# Execution

Usage:

~~~
    mpirun -np x ./bench_2decomp_fft <size-x> <size-y> <size-z>
~~~

Example:
~~~
mpirun -np 4 ./bench_2decomp_fft 128 128 128
~~~
* * *
