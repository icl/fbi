# Testing heFFTe

After successful compilation, go to heffte/build/benchmarks/. In theer, you can find the executables
`speed3d_c2c` and `speed3d_r2c`, respectively, for complex-to-complex and real-to-complex transforms.

Usage:

~~~
    mpirun -np x ./speed3d_c2c <backend> <precision> <size-x> <size-y> <size-z> <args>
~~~

# Flags and options explanation

~~~

backend is the 1-D FFT library
    available options for this build: cufft, fftw, mkl, rocm, onemkl 

precision is either float or double
    use float-long or double-long to enable 64-bit indexing

size-x/y/z are the 3D array dimensions 

args is a set of optional arguments that define 
algorithmic tweaks and variations
    -reorder: reorder the elements of the arrays so that each 1-D FFT will use contiguous data
    -no-reorder: some of the 1-D will be strided (non contiguous)
    -a2a: use MPI_Alltoallv() communication method
    -p2p: use MPI_Send() and MPI_Irecv() communication methods
    -pencils: use pencil reshape logic
    -slabs: use slab reshape logic
    -io_pencils: if input and output proc grids are pencils, useful for comparison with other libraries 
    -mps: for the cufft backend and multiple gpus, associate the mpi ranks with different cuda devices
~~~


Examples:
~~~
mpirun -np  4 ./speed3d_c2c fftw  double 128 128 128 -no-reorder
mpirun -np  8 ./speed3d_c2c cufft float  256 256 256
mpirun -np 12 ./speed3d_c2c fftw  double 512 512 512 -p2p -slabs
~~~
* * *

