# Execution

Usage:

~~~
    mpirun -np x ./fftw_c2c_mpi <size-x> <size-y> <size-z>
~~~

Example:
~~~
mpirun -np 4 ./fftw_c2c_mpi 128 128 128
~~~
* * *
