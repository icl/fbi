#include <fftw3-mpi.h>
#include <stdlib.h>
#include <math.h>


int main(int argc, char **argv){
  int rank  = 0;
  int size  = 1;
  int niter = 20;
  double *titer = NULL;
  double *tfwd  = NULL;
  double *tbwd  = NULL;
  double *max_tfwd = NULL;
  double *max_tbwd = NULL;
  double *min_tfwd = NULL;
  double *min_tbwd = NULL;
  ptrdiff_t L, M, N;
  double scaling = 0.0;
  fftw_plan planFwd;
  fftw_plan planBwd;
  MPI_Comm comm;

  if ( argc < 4 ) {
    fprintf ( stderr, "Error, the problem size must be given in the commandline\n" );
    return -1;
  }

  L = atoi(argv[1]);
  M = atoi(argv[2]);
  N = atoi(argv[3]);

  scaling = 1 / (  (double)L * M * N );

  fftw_complex *data;
  fftw_complex *ref;
  ptrdiff_t alloc_local, local_n0, local_0_start, i, j, k;

  MPI_Init(&argc, &argv);
  fftw_mpi_init();

  MPI_Comm_dup ( MPI_COMM_WORLD, &comm );

  MPI_Comm_rank ( comm, &rank );
  MPI_Comm_size ( comm, &size );

  titer     = (double*) malloc ( niter * sizeof(double));
  tfwd      = (double*) malloc ( niter * sizeof(double));
  tbwd      = (double*) malloc ( niter * sizeof(double));
  max_tfwd  = (double*) malloc ( niter * sizeof(double));
  max_tbwd  = (double*) malloc ( niter * sizeof(double));
  min_tfwd  = (double*) malloc ( niter * sizeof(double));
  min_tbwd  = (double*) malloc ( niter * sizeof(double));

  /*get local data size and allocate*/
  alloc_local = fftw_mpi_local_size_3d(L, M, N, comm,
                                        &local_n0, &local_0_start);

  data = fftw_alloc_complex(alloc_local);
  ref  = fftw_alloc_complex(alloc_local);

  /*create plan for out-of-place r2c DFT*/
  planFwd = fftw_mpi_plan_dft_3d(L, M, N, data, data, comm,
                                  FFTW_FORWARD, FFTW_ESTIMATE);

  planBwd = fftw_mpi_plan_dft_3d(L, M, N, data, data, comm,
                                  FFTW_BACKWARD, FFTW_ESTIMATE);

  srand(1);

  /*initialize rin to some functionmy_func(x,y,z) */
  for (i = 0; i < local_n0; ++i)
    for (j = 0; j < M; ++j)
      for (k = 0; k < N; ++k) {
        data[(i*M + j) * N + k][0] = rand() / ((double) RAND_MAX );
      //data[(i*M + j) * N + k][0] = 1.0;
      //data[(i*M + j) * N + k][1] = rand() / ((double) RAND_MAX ); 
        // For matching what heFFTe does and because I do not know how to compare two complex number, I set it to 0.
        data[(i*M + j) * N + k][1] = 0.0;
        ref [(i*M + j) * N + k][0] = data[(i*M + j) * N + k][0];
        ref [(i*M + j) * N + k][1] = 0.0;
      }


  /*compute transforms as many times as desired*/
  double tmp    = 0.0;
  double ti     = 0.0;
  double tn     = 0.0;
  double max_tn = 0.0;
  
  // Warmup
  fftw_execute(planFwd);
  fftw_execute(planBwd);

  // Normalization
  for (i = 0; i < local_n0; ++i)
    for (j = 0; j < M; ++j)
      for (k = 0; k < N; ++k) {
        data[(i*M + j) * N + k][0] *= scaling;
        data[(i*M + j) * N + k][1] = 0.0;
      }

  MPI_Barrier ( comm );
  for ( int iter = 0; iter < niter; ++iter ) {
    ti = MPI_Wtime();
    fftw_execute(planFwd);
    tfwd[iter] = MPI_Wtime() - ti;

    tmp = MPI_Wtime();
    fftw_execute(planBwd);
    tbwd[iter] = MPI_Wtime() - tmp;
    titer[iter] = MPI_Wtime() - ti;
    tn += titer[iter];

    // Normalization
    for (i = 0; i < local_n0; ++i)
      for (j = 0; j < M; ++j)
        for (k = 0; k < N; ++k) {
          data[(i*M + j) * N + k][0] *= scaling;
          data[(i*M + j) * N + k][1] = 0.0;
        }
    
    MPI_Barrier ( comm );
  }
//tn += MPI_Wtime();

  // Reduce operation to compute min, max and avg
  MPI_Reduce ( &tn, &max_tn, 1, MPI_DOUBLE, MPI_MAX, 0, comm );

  for ( int i = 0; i < niter; ++i ) {
    MPI_Reduce ( tfwd + i, max_tfwd + i, 1, MPI_DOUBLE, MPI_MAX, 0, comm );
    MPI_Reduce ( tbwd + i, max_tbwd + i, 1, MPI_DOUBLE, MPI_MAX, 0, comm );
    MPI_Reduce ( tfwd + i, min_tfwd + i, 1, MPI_DOUBLE, MPI_MIN, 0, comm );
    MPI_Reduce ( tbwd + i, min_tbwd + i, 1, MPI_DOUBLE, MPI_MIN, 0, comm );
  }

  // Compute local error
  double err = 0.0;
  double max_err = 0.0;
  for (i = 0; i < local_n0; ++i)
    for (j = 0; j < M; ++j)
      for (k = 0; k < N; ++k) {
      //printf ( "Ref[%ld] = %f\t data[%ld] = %f\n",
      //    (i*M + j) * N + k, ref[(i*M + j) * N + k][0],
      //    (i*M + j) * N + k, data[(i*M + j) * N + k][0] );
        double tmp_real = fabs(ref[(i*M + j) * N + k][0] - data[(i*M + j) * N + k][0]);
      //double tmp_img  = fabs(ref[(i*M + j) * N + k][1] - data[(i*M + j) * N + k][1]);
        if ( tmp_real > err )
          err = tmp_real;
      }

  MPI_Reduce ( &err, &max_err, 1, MPI_DOUBLE, MPI_MAX, 0, comm );

  // Computation of the flops
  double fwdAdd, fwdMul, fwdFma;
  double bwdAdd, bwdMul, bwdFma;
  fftw_flops ( planFwd, &fwdAdd, &fwdMul, &fwdFma );
  double fwdFlops = fwdAdd + fwdMul + fwdFma;
  fftw_flops ( planBwd, &bwdAdd, &bwdMul, &bwdFma );
  double bwdFlops = bwdAdd + bwdMul + bwdFma;

  if ( ! rank ) {
    printf ( "\n========================\nInput:" );
    for ( int i = 1; i < argc; ++i)
      printf ( " %s", argv[i] );
    printf ( "\n" );
    printf ( "FFTW3 test results\n---------\n"
        "Problem size:          %ldx%ldx%ld\n"
        "MPI ranks:             %d\n"
        "Execution(niter=%3d):  %.8fs\n"
        "GFlops:                %.3e\n"
        "Error:                 %.2e\n",
        L, M, N,
        size,
        niter, max_tn,
        ( fwdFlops + bwdFlops ) * niter / max_tn,
        max_err );

    // Print some details
    printf ( "Fwd details: max_i=[" );
    for ( int i = 0; i < niter; ++i )
      printf ( "%.6e ", max_tfwd[i] );
    printf ( "]\n" );
    printf ( "Fwd details: min_i=[" );
    for ( int i = 0; i < niter; ++i )
      printf ( "%.6e ", min_tfwd[i] );
    printf ( "]\n" );
    printf ( "Bwd details: max_i=[" );
    for ( int i = 0; i < niter; ++i )
      printf ( "%.6e ", max_tbwd[i] );
    printf ( "]\n" );
    printf ( "Bwd details: min_i=[" );
    for ( int i = 0; i < niter; ++i )
      printf ( "%.6e ", min_tbwd[i] );
    printf ( "]\n" );
  }

  fftw_destroy_plan(planFwd);
  fftw_destroy_plan(planBwd);

  free ( titer );
  free ( tfwd );
  free ( tbwd );
  free ( max_tfwd );
  free ( max_tbwd );
  free ( min_tfwd );
  free ( min_tbwd );

  MPI_Comm_free ( &comm );
  MPI_Finalize();

  return 0;
}
