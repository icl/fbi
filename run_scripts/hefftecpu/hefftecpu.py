#! /usr/bin/env python3

import sys

import numpy

import matplotlib.pyplot


def load(filename=__file__.replace(".py", ".txt")):
    return numpy.loadtxt(filename)


def plot(data, filename=__file__.replace(".py", ".pdf")):
    matplotlib.pyplot.set_loglevel("info")
    matplotlib.pyplot.cla()  # clear current axes
    matplotlib.pyplot.clf()  # clear current figure

    matplotlib.pyplot.loglog(data[:,0], data[:,1], "o:b", label="HeFFTe CPU", base=2)
    matplotlib.pyplot.loglog(data[:,0], data[:,2], "x:y", label="P3DFFT++ CPU", base=2)

    fig = matplotlib.pyplot.gcf()  # get current figure
    axs = fig.gca()  # get current axis
    axs.set_xscale("log", base=2)
    axs.set_yscale("log", base=10)

    matplotlib.pyplot.xlabel("Nodes")
    matplotlib.pyplot.ylabel("Time (seconds)")
    matplotlib.pyplot.grid(True, which="both", axis="both", linestyle=":") # which=major/minor/both axis=x/y/both
    matplotlib.pyplot.legend(loc="lower left", fancybox=True, shadow=True)
    matplotlib.pyplot.tight_layout()
    matplotlib.pyplot.savefig(filename, format="pdf")


def main(argv):
    plot(load())
    return 0


if "__main__" == __name__:
    sys.exit(main(sys.argv))
